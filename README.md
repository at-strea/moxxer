# Moxxer
## @**strea-nx/moxxer**
### @**strea/moxxer** upon release ###

A different take on dependency injection.

Moxxer is a performant cache for methods/objects that allows declarative, granular dependency injection. It's basic use case is to create a moxxer instance in any module to provide a scoped store for configurable settings and functionality.

## Basic Usage

---

`terminal`
```terminal
system:my_project me$ npm install @strea-nx/moxxer
```

`myModule.js`
```javascript
import Moxxer from './node_modules/@strea-nx/moxxer/index.js'

// [NOTE]: $DEPS is used as a convention (pending) but it can be called anything you want
const $DEPS = new Moxxer()
$DEPS.register('fetch', window.fetch)
export { $DEPS }

export default async function myExportedFunction ()
{
  let response
  try
  {
    response = await $DEPS.fetch('./config.json')
    return response.json()
  }
  catch (error)
  {
    ...
  }
  ...
}
```

`myModule.test.js`
```javascript
import { describe, it, expect, before } from './tests/interfaces.js'
import myExportedFunction, { $DEPS } from './myModule.js'

describe('does some stuff', function ()
{
  before(function ()
  {
    $DEPS.mock('fetch', function mockFetch (input, init)
    {
      return Promise.resolve({
        json: () =>
        {
          return '{"foo": "bar"}'
        }
      })
    })
  })

  it('resolves to `{"foo": "bar"}`', async function () 
  {
    const result = await myExportedFunction()
    expect(result).to.equal('{"foo": "bar"}')
  })
})
```
